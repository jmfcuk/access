import { NgModule } from '@angular/core';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { AuthService } from './auth.service';
import { Group1AuthGuard } from './group1-authguard';
import { Group2AuthGuard } from './group2-authguard';
import { Group3AuthGuard } from './group3-authguard';

@NgModule({
//   imports: [

//   ],
  declarations: [
	  UnauthorizedComponent
  ],
  providers: [
	  AuthService, Group1AuthGuard, Group2AuthGuard, Group3AuthGuard
  ],
})
export class AuthModule { }

