export interface IUserInfo {

	username: string;
	groups: Array<string>;
}

