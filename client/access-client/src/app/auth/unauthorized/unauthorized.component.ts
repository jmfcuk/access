import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'unauthorized',
  templateUrl: './unauthorized.component.html'
})
export class UnauthorizedComponent {

  msg: string;

  constructor(route: ActivatedRoute) {
    this.msg = 'Unauthorized for ' + route.snapshot.params['src'];
  }
}

