import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class Group3AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(destination: ActivatedRouteSnapshot,
                state: RouterStateSnapshot) {

		let b: boolean = this.authService.isInGroup('group3');

        let src = state.url;

        if(!b)
            this.router.navigateByUrl('/unauthorized' + src);

        return b; 
    }
}



