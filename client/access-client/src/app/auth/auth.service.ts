import { Injectable } from '@angular/core';
import { Observable, Subscription, Subject } from 'rxjs';
import { Http, Headers, Response } from '@angular/http';
import { IUserInfo } from './iuserinfo';

@Injectable()
export class AuthService extends Subject<IUserInfo> {

	// username: Observable<string>;
	// groups: Observable<Array<string>>;

	// username: string;
	// groups: Array<string>;

	user: IUserInfo;

	constructor(private http: Http) {
		super();

		this.getUserInfo();
	}

	private getUserInfo(): void {

		let url = 'http://hnpc06414:8888/api/test/getusergroups';

		let headers = new Headers();

		headers.append('Accept', 'application/json');

		this.http.get(url, { withCredentials: true, headers: headers })
			.map((res: Response) => { return res.json(); })
			.subscribe((data: IUserInfo) => {
				this.user = data;
				console.log('service username = ' + this.user.username);
				console.log('service groups = ' + this.user.groups);

				super.next(this.user);
			},
			err => console.error(err),
			() => console.log('done')
			);
	}

	isInGroup(group: string): boolean {

		let b: boolean = false;

		console.log('AuthService::isInGroup');

		if (this.user) {
			for (let g of this.user.groups) {

				console.log('AuthService::isInGroup(' + group + ') === ' + g);

				if (g === group) {
					b = true;
					break;
				}
			};
		}

		return b;
	}
}

