
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { Test1Component } from './test1/test1.component';
import { Test2Component } from './test2/test2.component';
import { UnauthorizedComponent } from './auth/unauthorized/unauthorized.component';
import { Group1AuthGuard } from './auth/group1-authguard';
import { Group2AuthGuard } from './auth/group2-authguard';
import { Group3AuthGuard } from './auth/group3-authguard';

const appRoutes: Routes = [
  { path: 'test1', component: Test1Component, canActivate: [Group1AuthGuard] },
  { path: 'test2', component: Test2Component, canActivate: [Group2AuthGuard] },
  { path: 'test3', component: Test2Component, canActivate: [Group3AuthGuard] },
  { path: 'unauthorized/:src', component: UnauthorizedComponent },
  { path: '', component: HomeComponent }
];

export const appRoutingProviders: any[] = [
  
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
