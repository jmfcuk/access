import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { IUserInfo } from './auth/iuserinfo';
import { Http, Headers, Response } from '@angular/http';

@Component({
  selector: 'access-client-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  username: string;
  groups: Array<string>;

  //user: IUserInfo;

  constructor(private authService: AuthService) {

    this.authService.subscribe(user => {
      this.username = user.username;
      this.groups = user.groups;
      console.log('app username = ' + this.username);
      console.log('app groups = ' + this.groups);
    });
  }

  ngOnInit(): void {

    // this.authService.subscribe(user => {
    //   this.username = user.username;
    //            this.groups = user.groups;
    // 					 console.log('app username = ' + this.username);
    // 					 console.log('app groups = ' + this.groups);
    //});

    // .((data: IUserInfo) => {
    // 					 this.username = data.username;
    //            this.groups = data.groups;
    // 					 console.log('app username = ' + this.username);
    // 					 console.log('app groups = ' + this.groups);
    // },
    // err => console.error(err),
    // () => console.log('done')
    // );
  }
}
