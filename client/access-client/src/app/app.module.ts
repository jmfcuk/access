import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { routing, appRoutingProviders }  from './app.routing';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { Test1Component } from './test1/test1.component';
import { Test2Component } from './test2/test2.component';
import { Test3Component } from './test3/test3.component';
import { AuthModule } from './auth/auth-module';
// import { UnauthorizedComponent } from './auth/unauthorized/unauthorized.component';
// import { AuthService } from './auth/auth.service';
// import { Group1AuthGuard } from './auth/group1-authguard';
// import { Group2AuthGuard } from './auth/group2-authguard';
// import { Group3AuthGuard } from './auth/group3-authguard';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    AuthModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    Test1Component,
    Test2Component,
    Test3Component,
    //UnauthorizedComponent
  ],
  providers: [
    appRoutingProviders//, AuthService, Group1AuthGuard, Group2AuthGuard, Group3AuthGuard
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
