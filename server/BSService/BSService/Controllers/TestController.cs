﻿using System.Linq;
using System.Security.Principal;
using System.Web.Http;

namespace BSService.Controllers
{
	public class UserInfo
	{
		public string username;
		public string[] groups;
	}

	[Authorize]
	public class TestController : ApiController
    {
		public UserInfo GetUserGroups()
		{
			UserInfo ui = null;

			var p = RequestContext.Principal;

			WindowsIdentity wi = p.Identity as WindowsIdentity;

			var groups =
				from sid in wi.Groups
				select sid.Translate(typeof(NTAccount)).Value;

			ui = new UserInfo()
			{
				username = wi.Name,
				groups = groups.ToArray()
			};

			return ui;
		}
	}
}
